FROM node:14

ENV PORT 5000

WORKDIR /app

COPY package*.json ./
# install dev dependencies also for client side
RUN npm install

COPY . .
ENV NODE_ENV production
RUN npm run build \
    # remove dev dependencies
    && npm prune

EXPOSE 5000

CMD ["npm", "start"]
